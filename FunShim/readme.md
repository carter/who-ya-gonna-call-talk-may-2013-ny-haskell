

do 

./waf.py configure
./waf.py build

then ./runBench.sh and enjoy getting benchmark data

the waf.py and wscript (also python) are courtesy of the alphaheavy 
example found  at https://github.com/alphaHeavy/kospi
I would have liked to change the build script to using Shake, but didn't find the time.
patches welcome for that change to remove the python code and replace it with shake :) 


for a pretty graphical version of a benchmark run, look inside the Benchs/ directory
after running ./runBench.s