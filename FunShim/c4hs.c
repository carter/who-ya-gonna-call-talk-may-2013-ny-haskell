#include <stdint.h>

#define GHC_ARGS int64_t* restrict baseReg, \
    int64_t* restrict sp, \
    int64_t* restrict hp, \
    int64_t  r1, \
    int64_t  r2, \
    int64_t  r3, \
    int64_t  r4, \
    int64_t  r5, \
    int64_t  r6, \
    int64_t* restrict spLim, \ 
    float f1, \ 
    float f2, \
    float f3, \
    float f4, \
    double d1, \
    double d2 \ 

typedef void (*HsCall)(int64_t*, int64_t*, int64_t*, int64_t, int64_t, int64_t, int64_t, int64_t, int64_t, int64_t*, float, float, float, float, double, double);


//      HsCall Retfun = (HsCall)sp[0];

static inline void
returnN(
    HsCall fun,
    int64_t* restrict baseReg,
    int64_t* restrict sp,
    int64_t* restrict hp,
    int64_t* restrict spLim,
    int64_t val)
{
    // create undefined variables, clang will emit these as a llvm undef literal
    const int64_t iUndef;
    const float fUndef;
    const double dUndef;

    return fun(
        baseReg,
        sp,
        hp,
        val,
        iUndef,
        iUndef,
        iUndef,
        iUndef,
        iUndef,
        spLim,
        fUndef,
        fUndef,
        fUndef,
        fUndef,
        dUndef,
        dUndef);
}

extern void c4hs_doubleWord(GHC_ARGS){
     HsCall Retfun = (HsCall)sp[0];
     int64_t  val = r1 ; 
     val = val + val ; 
     return returnN(Retfun, baseReg,sp,hp, spLim, val ) ; 

}

extern void c4hs_Factorial(GHC_ARGS){
     HsCall Retfun = (HsCall)sp[0];
    unsigned long num = r1 ; 
     unsigned long accum = num ; 
    num -- ;
    while(num > 0){
        accum *= num ;
        num -- ; 

    }
    return   returnN(Retfun, baseReg,sp,hp, spLim, accum ) ; 

}

