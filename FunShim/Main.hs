{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE GHCForeignImportPrim #-}
{-# LANGUAGE MagicHash #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE UnboxedTuples  #-}
{-# LANGUAGE UnliftedFFITypes #-}
{-# LANGUAGE CPP  #-}

module Main where


import GHC.Word
import Criterion.Main
import Data.Foldable hiding (mapM_)
import Criterion.Config
import Data.Word (Word64)
import Foreign.ForeignPtr (withForeignPtr)
import GHC.Prim
import GHC.Ptr (Ptr(..))
import GHC.Types

import GHC.Word
import Foreign
import Foreign.C.Types
import Foreign.Ptr
import System.IO.Unsafe (unsafeDupablePerformIO)

import GHC.ForeignPtr
import Control.Monad.Primitive

import qualified  Data.Vector.Storable.Mutable as SM 

{-  Word64 = W64# Word# , who knew? #-}



{-# NOINLINE hsDoubleWord64 #-}
hsDoubleWord64 :: Word64 -> Word64
hsDoubleWord64 !w =  case  w + w of 
                            !r -> r 

{-# NOINLINE hsFactorial #-}
hsFactorial :: Word64 -> Word64
hsFactorial !n = go n (n-1) 
    where   go !accum !0  = accum
            go !accum !ix = go (accum * ix ) (ix - 1)
{-# INLINE hsFactorialInlined #-}
hsFactorialInlined :: Word64 -> Word64
hsFactorialInlined !n = go n (n-1) 
    where   go !accum !0  = accum
            go !accum !ix = go (accum * ix ) (ix - 1)


{-# INLINE hsDoubleWord64Inlined #-}
hsDoubleWord64Inlined :: Word64 -> Word64
hsDoubleWord64Inlined !w =  case  w + w of 
                            !r -> r                             
-- inlining the wrapper to make things more fair  
{-# INLINE wrapped_cmmDoubleWord #-}
wrapped_cmmDoubleWord :: Word64 -> Word64
wrapped_cmmDoubleWord  !(W64# wnum) =  W64# res 
    where 
       !res = (cmm_doubleWord wnum)

wrapped_C4HS_doubleword   !(W64# wnum) =  W64# res 
    where 
       !res = (c4hs_doubleWord wnum)
      

foreign import prim "cmm_doubleWord" cmm_doubleWord  :: Word# -> Word#

foreign import prim "cmm_factorialWord" cmm_factorialWord  :: Word# -> Word# 


{-# INLINE wrapped_cmm_factorialWord #-}
wrapped_cmm_factorialWord  (W64# n) =  W64# res 
    where 
        res =  cmm_factorialWord   n 

foreign import ccall unsafe "normal.c c_doubleNum" 
    c_doubleNum_unsafeFFI:: CULong ->CULong 
{-# INLINE wrapper_c_doubleNum_UNSAFE #-}
wrapper_c_doubleNum_UNSAFE :: Word64 -> Word64  
wrapper_c_doubleNum_UNSAFE num  =  fromIntegral $! (c_doubleNum_unsafeFFI $! fromIntegral num   )


foreign import ccall safe "normal.c  c_doubleNum_alt" 
    c_doubleNum_safeFFI:: CULong ->CULong 

foreign import ccall unsafe "normal.c c_factorialNum"
    c_factorialNumUNSAFE ::  CULong -> CULong

{-#  INLINE wrapper_c_factorial_UNSAFE #-}
wrapper_c_factorial_UNSAFE :: Word64 -> Word64
wrapper_c_factorial_UNSAFE   !x  = fromIntegral (c_factorialNumUNSAFE $! fromIntegral x  )

foreign import prim "c4hs_Factorial" c4hs_Factorial :: Word# -> Word#

{-# INLINE wrapped_C4HS_factorial #-}
wrapped_C4HS_factorial (W64# num) = W64# res
    where res = c4hs_Factorial num 

{-# INLINE wrapper_c_doubleNum_SAFE #-}
wrapper_c_doubleNum_SAFE :: Word64 -> Word64  
wrapper_c_doubleNum_SAFE num  =  fromIntegral $! (c_doubleNum_safeFFI $! fromIntegral num   )



foreign import prim "c4hs_doubleWord" c4hs_doubleWord :: Word# ->Word#
main =  defaultMainWith defaultConfig{cfgSamples=ljust 100} (return ()) [
        bgroup "double a  64bit word" [
            bcompare [ bench "wrapped_cmmDoubleWord 1000" $! whnf wrapped_cmmDoubleWord 7 ,
                        bench "hs double word  1000" $! whnf hsDoubleWord64 7 ,
                        bench "hs double word INLINED 1000" $! whnf hsDoubleWord64Inlined 7 ,
                        bench "hs double int  C4hs  1000" $!  whnf  wrapped_C4HS_doubleword 7,
                        bench " double int  C UNSAFE  1000" $!  whnf  wrapper_c_doubleNum_UNSAFE 7,
                        bench " double int  C SAFE 1000" $!  whnf  wrapper_c_doubleNum_SAFE 7
                        ]],
        bgroup "factorial   64bit word" [
            bcompare [  bench "wrapped_cmm_factorialWord " $! whnf wrapped_cmm_factorialWord 10,
                       
                        bench "hs factorial word  " $! whnf hsFactorial 10,
                        bench "hs factorial  word INLINED " $! whnf hsFactorialInlined 10,
                        bench "factorial   C4hs  " $!  whnf wrapped_C4HS_factorial 10 ,
                        bench "factorial unsafe C FFI"  $! whnf wrapper_c_factorial_UNSAFE 10 
                            ]   ] 


        ] 






{-# INLINE mallocAlignedVectorAVX #-}
mallocAlignedVectorAVX :: Storable a => Int -> IO (ForeignPtr a)
mallocAlignedVectorAVX =
#if __GLASGOW_HASKELL__ >= 605
    doMalloc undefined
        where
          doMalloc :: Storable b => b -> Int -> IO (ForeignPtr b)
          doMalloc dummy size = mallocPlainForeignPtrBytesAlignedAVX (size * sizeOf dummy)
#else
    mallocForeignPtrArray
#endif


-- | This function is similar to 'mallocForeignPtrBytes', except that
-- the internally an optimised ForeignPtr representation with no
-- finalizer is used. Attempts to add a finalizer will cause an
-- exception to be thrown.

-- should look into using 

mallocPlainForeignPtrBytesAlignedAVX :: Int -> IO (ForeignPtr a)
mallocPlainForeignPtrBytesAlignedAVX !(I# size) = IO $ \s ->
        case newAlignedPinnedByteArray# size align s      of { (# s', mbarr# #) ->
           (# s', ForeignPtr (byteArrayContents# (unsafeCoerce# mbarr#))
                             (PlainPtr mbarr#) #)
         }
    where 
        !(I# align) =  16 --32 -- 256/8 = 32 , 16byte alignment should be enough though


{-# INLINE basicUnsafeNewAVX #-}
basicUnsafeNewAVX n  = unsafePrimToPrim
    $ do
        fp <- mallocAlignedVectorAVX n
        return $ SM.MVector n fp

{-# INLINE replicateAlignedAVX #-}
replicateAlignedAVX ::(PrimMonad m, Storable a) => Int -> a -> m (SM.MVector (PrimState m) a)
replicateAlignedAVX size init =
        do  res <-  basicUnsafeNewAVX size
            SM.set res init
            return res             