to build your own copy of the talk,
have pandoc installed (plus pdflatex)

then just run ./build.sh

the /FunShim directory has all the demo code and associated (very hacky)
build scripting are partially written using https://github.com/alphaHeavy/kospi
examples, plus reading the primops.cmm file in ghc 7.6.3  https://github.com/ghc/ghc/blob/ghc-7.6/rts/PrimOps.cmm



All code in the Funshim directory is property of the respective authors, that said,
do whatever you want with it, and if licensing matters, talk with me and I'll sort out 
getting the relevant code permissively licensed.

 