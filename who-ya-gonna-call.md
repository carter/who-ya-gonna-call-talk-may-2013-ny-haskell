% Who Ya Gonna Call: GHC and it's Primops
% Carter Schonwald
% Haskell NYC User Group 

#  Goals for this Talk

* Understand (roughly)  how functions are compiled / implemented in GHC
* Learn about the Continuation Passing Style (CPS) Transform
    - Also understand the pragmatics of *Why* CPS is useful in a compiler
* Learn how to write new primops that GHC can use and that may interact with 
the haskell RTS.
    - and how to write them in GHC's implementation of C Minus Minus, or perhaps
    even C!


# The Architecture of GHC

![ghc arch](./ghc-architecture-shrunk.png)

# Lets focus on a subset of a subset

![subset](./ghc-architecture-focused.png)

# Lets focus some more! (how we reach C Minus Minus)

* Ignore optimization completely

* Kinda Gloss over evaluation strategy 

* Lets actually just work though it for the untyped lambda calculus

* ANF, CPS, and Closure conversion, with working code

# Interpretors, Semantics, and Bound

* All my example code in haskell will use the excellent Bound library by 
    Edward Kmett, If you're messing with interpreters etc, worth using it



# Bound makes a very nice connection between subsitution and monads.

* let ```e :: M a``` be some expression with free variables ``` v :: a ```

* suppose we have a function representing the mapping from variables to expressions/values
``` f :: a -> M a```

* then substitution  is applying ```f``` over all the variables in ``` e```

* wait, that means ```subst :: M a -> (a -> M b) -> M b ```

* Substitution is a monad! So lets use bind ```>>=``` instead of ```subst```


# Some Boiler Plate
\begin{code}
    {-# LANGUAGE DeriveFunctor, DeriveFoldable, DeriveTraversable #-}
    import Bound
    import Control.Applicative
    import Control.Monad (ap)
    import Prelude.Extras
    import Data.Foldable
    import Data.Traversable
\end{code}

# Heres the Untyped Lambda Calculus

\begin{code}
infixl 9 :@
data Exp a = V a 
    | Exp a :@ Exp a 
    | Lam (Scope () Exp a)
deriving 
    (Eq,Ord,Show,Read,Functor,Foldable,Traversable)

instance Eq1 Exp
instance Ord1 Exp
instance Show1 Exp
instance Read1 Exp
instance Applicative Exp where pure = V; (<*>) = ap    
\end{code}


# Now Some substitution

\begin{code}
instance Monad Exp where
   return = V
   V a      >>= f = f a
   (x :@ y) >>= f = (x >>= f) :@ (y >>= f)
   Lam e    >>= f = Lam (e >>>= f)
   --- note that (>>>=) != (>>=), 
   --- look it up  :) 
\end{code}

* here ```exp >>= f``` is recursively applying the ```f:: a -> M b``` substituion. 

* can do a nice shorthand for writing lambda with 
\begin{code}
 lam :: Eq a => a -> Exp a -> Exp a
 lam v b = Lam (abstract1 v b)
\end{code}

# Heres a Toy Evaluator

\begin{code}
whnf :: Exp a -> Exp a
 whnf (f :@ a) = case whnf f of
   Lam b -> whnf (instantiate1 a b)
   f'    -> f' :@ a
 whnf e = e
\end{code}

* Notice: we're substituting under lambdas with our current ```>>=```,
    and when we run compiled things, we don't generally copy and paste values under lambda!

# Lets Wrap this problem up, with Closures!

* lets change some of those defns from earlier

* A Closure is a lambda with the substitution we care about saved with it
\begin{code}   
data Exp a = 
    ...
    | Lam (Scope () Exp a)
    | Closure (a -> Exp a)  (Scope () Exp a) 
deriving 
    (Eq,Ord,Show,Read,Functor,Foldable,Traversable)
\end{code}

# But how do we get to a closure?

\begin{code}
instance Monad Exp where
   ...
   Lam e    >>= f = Closure f  e 
   --- save the substitution for when we enter it
   c@Closure{...} >>= f = c
   --- a closure is an abstraction barrier
\end{code}

* Closures close, accept no substitutes!

# Analogous evaluator 

\begin{code}
whnf :: Exp a -> Exp a
 whnf (f :@ a) = case whnf f of
    Closure f b  -> 
        whnf (instantiate1 a $  (b >>>= f  ))
    --- heres the lambda one again, 
    ---shouldn't happen though!
    Lambda  b  -> whnf (instantiate1 a  b)
   f'    -> f' :@ a
 whnf e = e

 toplevelWHNF e =  whnf $! ( e >>= id )
 --- closes the toplevel environment, with closures!
\end{code}


# Bah, Boring and not an interesting evaluator

* we're using the haskell stack implicitly to support a call stack in this lang!

* lets make our evaluator have its own little stack of contexts, have 
    every function only appear in tail position.

* heres an idea : explicit stack

\begin{code}
data StackMachine  a = 
    SMach (Exp a -> [StackMachine a] -> Exp a )
\end{code}

# Heres its Eval (and apply)

\begin{code}
toplevelWHNF e = stackWHNF (e >>= id ) []

stackApply  e [] = e 
stackApply e  (SMach a : as ) = a e as

stackWHNF:: Exp a -> [StackMachine a] -> Exp a
stackWHNF (f @: a) ls = stackWHNF f (SMach (  \e -> 
 case e of 
    Closure f b  -> 
        stackWHNF (instantiate1 a $  (b >>>= f  ) )
                     ls 
   
    f'    -> stackApply (f' :@ a) ls  ) : ls )
stackWHNF e ls = stackApply e ls 
\end{code}


#That Looked Weird

* thats what happs when you dont do Continuation Passing Style Transformation

* because we do need a stack somewhere

* whats the rough idea for CPS?


# CPS transform in a page (an abuse of a beautiful topic)
## note this is pseudocode, not actual Haskell. 
\begin{code}

toCPStoplevel e = toCPS e TOP

toCPS (app e1 e2) k = toCPS e1 \v1 -> 
                        toCPS e2 \v2-> v1 v2 k
toCPS (lam x-> e) k = k (lam x k -> toCPS e k )
toCPS (variable v)  k = k v  
\end{code}
* when we don't augment K

# Why Did I show you this?

* Continuations makes stack Allocation explicit as a programmatic construct you can optimize
* Closures tell us what values to persist in the heap / stack
* the Closure part of a continuation tells us what values to save on the stack 

# Why did i tell you this?

* CMM (C minus minus), the last IR before LLMV / Native code generation
is CPSd, and Closure Converted

* CMM is kinda C without a type system, or assembly with better syntax
    - theres a C Minus Minus language too, but GHC has its own version

# How do funcalls work at the CMM level?

* GHC has its own calling convention to support tail calls efficiently.

* Let me show you the x86_64 calling convention for ghc 7.6

* Has 6 registers reserved for Words, 

* 4 registers for Base , heap , stack pointers, plus stack limit  pointer

* 2 SIMD registers for Doubles

* 4 SIMD registers for Floats 

* any more arguments are put on the Stack in order

* the Calling Convention will change slightly in GHC 7.8

# What are those fixed registers?
* From the SOURCE

\begin{code}
REG_Base  r13
REG_Sp    rbp
REG_Hp    r12
REG_R1-R6    rbx r14 rsi rdi r8 r9
REG_SpLim r15
REG_F1-F4    xmm1-xmm4
REG_D1    xmm5
REG_D2    xmm6
\end{code}

# LLVM has GHC calling convention included

\begin{code}
def CC_X86_64_GHC : CallingConv<[
  // Promote i8/i16/i32 arguments to i64.
  CCIfType<[i8, i16, i32], CCPromoteToType<i64>>,

  // Pass in STG registers: Base, Sp, Hp, R1, 
                R2, R3, R4, R5, R6, SpLim
  CCIfType<[i64],
            CCAssignToReg<[R13, RBP, R12, RBX,
                 R14, RSI, RDI, R8, R9, R15]>>,

  // Pass in STG registers: F1, F2, F3, F4, D1, D2
  CCIfType<[f32, f64, v16i8, v8i16, v4i32, 
                        v2i64, v4f32, v2f64],
            CCIfSubtarget<"hasSSE1()",
            CCAssignToReg<[XMM1, XMM2, XMM3, 
                    XMM4, XMM5, XMM6]>>>
]>;
\end{code}

#Why on earth would you use CMM instead of C or haskell?

* Maybe you want to have multiple values AND interact with the RTS
    - This is how many of the concurrency primops are implemented

* You like writing assembly

* its pretty fast

# DEMO (code available online)

How does CMM compare with Haskell code, C using the FFI's,
and C modified to use the GHC calling convention


# Conclusions from measurements (added after talk) 

* Inlined Haskell can beat  CMM or C.

* use unsafe C ffi for sub-millisecond codes, the calling overhead is less than 5ns!
    
    - for longer work, unsafe is unsafe! The call blocks that ghc thread from doing anything else till it returns.

* use safe C ffi for any long running ffi operations, runs in a seperate thread meant for such work.

* CMM or the LLVM modified C that uses the GHC calling convention is only suitable when you 
need to either hook into the GHC Run Time System, or do multiple return values from the foreign call.

    - Example of the first use case might be writing a concurrency primitive for atomic writes to an array

    - Example of the latter might be returning multiple fields from a network packet as an unboxed tuple.

# References and notes, added after the talk

* Safe FFI C Calls from Haskell are run in their own thread, I incorrectly state 
in the recorded talk that Safe C calls run in a separate process

* To learn more about CPS and Abstract Machines, the collected works
of Olivier Danvy are a great start http://www.cs.au.dk/~danvy/index-previous.html 

* To learn about Compiling (Strict) Functional Languages, the text "Compiling with Continuations" by 
Andrew Appel is a great classic / starting resource

* Compiling a lazy language is a bit more subtle: "Implementing functional languages: a tutorial"  is a classic,
avialable here http://research.microsoft.com/en-us/um/people/simonpj/papers/pj-lester-book/


# Notes and References, Continued

* Staring at parts of the GHC source and trying to help out is another great way to learn A LOT 
about what goes into a modern optimizing compiler for a functional programming language.

    - Seriously, consider it! :)  


# Thank You!    

<!-- # WHat else
https://github.com/ghc/ghc/blob/master/includes/stg/MachRegs.h
then introduce the version with closures, (thats closure conversion after all)

then lets 

then continuations

(each step motivated by making the evaluator more "practical") -->
<!-- https://github.com/llvm-mirror/llvm/blob/master/lib/Target/X86/X86CallingConv.td
 -->
